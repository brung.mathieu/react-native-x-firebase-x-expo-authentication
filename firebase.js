// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDV5cmPWGDQgjbrJPlV2-5n4nH70vmdMNw",
    authDomain: "reactnative-x-firebase.firebaseapp.com",
    projectId: "reactnative-x-firebase",
    storageBucket: "reactnative-x-firebase.appspot.com",
    messagingSenderId: "704761706321",
    appId: "1:704761706321:web:4f36df2e447556dac10570"
};

export const app = initializeApp(firebaseConfig);
