import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { getAuth } from "firebase/auth";
import {app} from "../firebase";


const HomeScreen = ({ navigation }) => {

    const onSignOut = () => {
        getAuth()
            .signOut()
            .then(() => {
                console.log("Logged out")
                navigation.replace("Login");
            })
            .catch(error => alert(error.message))
    }

    return (
        <View style={styles.container}>
            <Text>Email : { getAuth().currentUser?.email }</Text>
            <TouchableOpacity
                onPress={onSignOut}
                style={styles.button}
            >
                <Text
                    style={styles.buttonText}
                >
                    Sign out
                </Text>
            </TouchableOpacity>
        </View>
    )
}

export default HomeScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        backgroundColor: '#FFA200',
        width: '60%',
        padding: 15,
        borderRadius: 10,
        alignItems: 'center',
        marginTop: 40
    },
    buttonText: {
        color: '#fff',
        fontWeight: '700',
        fontSize: 16
    },
})