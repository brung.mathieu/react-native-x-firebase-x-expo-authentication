import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text, TextInput, TouchableOpacity } from "react-native";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, onAuthStateChanged } from "firebase/auth"
import {app} from "../firebase";

const LoginScreen = ({ navigation }) => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    useEffect(() => {
        const auth = getAuth();
        const unsubscribe =  onAuthStateChanged(auth, (user) => {
            if (user) {
                const userId = user.uid;
                navigation.replace("Home");
            }
        })

        return unsubscribe;
    }, [])

    const onRegister = () => {
        const auth = getAuth();
        createUserWithEmailAndPassword(auth, email, password)
            .then(userCredentials => {
                const user = userCredentials.user;
                console.log('Registered with : ', user.email);
            })
            .catch(error => alert(error.message))
    }

    const onLogin = () => {
        const auth = getAuth();
        signInWithEmailAndPassword(auth, email, password)
            .then((userCredentials) => {
                const user = userCredentials.user;
                console.log("Logged in with : ", user.email);
            })
            .catch(error => alert(error.message));
    }

    return (
        <View style={styles.container}>
            <View style={styles.titleContainer}>
                <View style={styles.title}>
                    <Text style={styles.titleText}>
                        React Native x Firebase
                    </Text>
                    <Text style={styles.titleText}>
                        Authentication
                    </Text>
                </View>
            </View>

            <View style={styles.inputButtonContainer}>
                <View style={styles.inputContainer}>
                    <TextInput
                        placeholder="Email"
                        value={email}
                        onChangeText={text => setEmail(text)}
                        style={styles.input}
                        autoCapitalize='none'
                        autoComplete='email'
                    />
                    <TextInput
                        placeholder="Password"
                        value={password}
                        onChangeText={text => setPassword(text)}
                        style={styles.input}
                        secureTextEntry
                        autoCapitalize='none'
                    />

                </View>

                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        onPress={onLogin}
                        style={styles.button}
                    >
                        <Text style={styles.buttonText}>Login</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={onRegister}
                        style={[styles.button, styles.buttonOutline]}
                    >
                        <Text style={styles.buttonOutlineText}>Register</Text>
                    </TouchableOpacity>

                </View>
            </View>
        </View>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 50,
        justifyContent: "space-around",
        alignItems: 'center',
    },
    inputButtonContainer: {
        width: '100%',
        justifyContent: "center",
        alignItems: 'center',
        marginBottom: 100,
    },
    titleContainer: {
        width: '60%',
        marginTop: 100,
        justifyContent: 'center',
        alignItems: "center",
    },
    title: {
        justifyContent: 'center',
        alignItems: "center",
    },
    titleText: {
        color: '#FFA200',
        fontWeight: '700',
        fontSize: 20
    },
    inputContainer: {
        width: '80%',
        marginBottom: 50,
    },
    input: {
        backgroundColor: '#fff',
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderRadius: 10,
        marginTop: 5,
    },
    buttonContainer: {
        width: '70%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        backgroundColor: '#FFA200',
        width: '100%',
        padding: 15,
        borderRadius: 10,
        alignItems: 'center'
    },
    buttonText: {
        color: '#fff',
        fontWeight: '700',
        fontSize: 16
    },
    buttonOutline: {
        backgroundColor: '#fff',
        marginTop: 5,
        borderColor: '#FFA200',
        borderWidth: 2,
    },
    buttonOutlineText: {
        color: '#FFA200',
        fontWeight: '700',
        fontSize: 16
    }
})